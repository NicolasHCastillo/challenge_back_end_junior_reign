# Challenge - Junior Back End Developer, Reign

## Welcome!

This is my proposed solution for the test `Junior Back End Developer`!

The technologies used were:

- NodeJS
- MongoDB

## Prerequisites

To use these files, you must first have the following installed:

- [Git](https://git-scm.com/)
- [NodeJs LTS](https://nodejs.org/es/)

## How to use

1. Clone this repository.

```bash
git clone https://gitlab.com/NicolasHCastillo/challenge_back_end_junior_reign.git
```

2. Change directory into the root of the project.

```bash
cd  challenge_back_end_junior_Reign 
```

3. Build and Run the project with one of the follows command:
```bash
npm install
```
When the install done, the server can now be started with the next command: 
```bash
npm start
```

### Note: Before start the server, make sure that the variant MONGO_CONNECTION in the file .env be valid, if not, insert one that be 


API Resources:
 - GET /api/article - Filter articles by author, _tags and title
 - DELETE /api/article/:id - Delete article from the data base using a valid mongoId

For view the documentation of the API resources, start the server and go to [http://localhost:8080/api-docs](http://localhost:8080/api-docs) in your browser.


## Test and Coverage

If you need to execute the test for the solution backend and generate the coverage report, then execute one of the follows commands:
```bash
npm run test
```

Have a nice day. I really enjoyed solving this challenge :)
