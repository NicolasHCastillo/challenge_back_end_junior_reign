const express = require('express')
const { response } = require('express')
const Article = require('../models/article.models')


const optionPaginate = {
    limit: 5
}

const filterArticles = async (req, res = response) =>{
    let {author, _tags, title, page=1} = req.query
    author ? author = RegExp(author, 'i') : author = [null, /./]
    if(!_tags){
        _tags = [null, /./]
    }else{
        arrTags = _tags.split(',')
        _tags = arrTags.map(tag => RegExp(tag, 'i'))
    }
    if(!Number(page)) return res.status(400).json({msg:"page most be a number"})
    title ? title = RegExp(title, 'i') : title = [null, /./]
    try{
        const data = await Article.paginate({
            '_highlightResult.author.value': author,
            title:{$in: title},
            _tags:{$in:_tags},
            delete:  false
        }, {...optionPaginate, page})
        res.status(200).json({ data })
    }catch(err){
        throw new Error(err)
    }
}

const deleteArticle = async (req, res=response) => {
    const {id} = req.params
    const article = await Article.findByIdAndUpdate(
        id,
        {delete:true},
        {new:true})
    if(!article) return res.status(404).json({msg:"Article not found"})
    res.status(200).json({msg:`The article with id ${id} was delete`})
}

module.exports= {
    filterArticles,
    deleteArticle
}
