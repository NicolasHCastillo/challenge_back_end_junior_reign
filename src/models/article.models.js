const mongoosePaginate = require('mongoose-paginate-v2');
const {Schema, model} = require('mongoose')

const ArticleSchema = Schema({
    created_at: {
        type: String,
    },
    delete: {
        type: Boolean,
        default: false,
        required: true
    },
    title: {
        type: String,
    },
    url: {
        type: String
    },
    points: {
        type: Number
    },
    story_text: {
        type: String
    },
    comment_text: {
        type: String
    },
    num_comments: {
        type: Number
    },
    story_id: {
        type: Number
    },
    story_title: {
        type: String
    },
    story_url: {
        type: String
    },
    parent_id: {
        type: String
    },
    created_at_i: {
        type: String
    },
    _tags: [{
        type: String
    }],
    objectID: {
        type: String,
        unique: true
    },
    _highlightResult: {
        type: Object
    }
})

ArticleSchema.plugin(mongoosePaginate)

ArticleSchema.methods.toJSON = function(){
    const { __v, ...article} =  this.toObject()
    return article
}

module.exports = model('Article', ArticleSchema)
