require('dotenv').config()
const express = require('express')
const axios = require('axios').default
const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUI = require('swagger-ui-express');
const {dbConnection} = require('../db/config.db')
const Article = require('./article.models')
class Server{
    constructor(){
        this.app = express()
        this.port = process.env.PORT

        this.paths = {
            article: '/api/article',
            docs: '/api-docs'
        }

        this.connectDB()
        this.middleware()

        this.swaggerOptions = {
            swaggerDefinition: {
              info: {
                title: "Challenge - Junior Back End Developer",
                version: '1.0.0',
              },
              servers: [
                {
                  url: `http://localhost:${this.port}`,
                  description: 'Development server',
                },
              ],
              tags: [
                {
                    "name": "Articles API",
                }
            ]
            },
            apis: [`${__dirname}/../routes/*.js`]
        };
        this.swaggerDocs = swaggerJsDoc(this.swaggerOptions)
        this.routes()
    }

    async connectDB(){
        await dbConnection()
        this.requestPage()
        this.requestBucle()
    }

    middleware(){
        this.app.use(express.json())
        this.app.use(express.static('public'))
    }
    routes(){
        this.app.use(this.paths.article, require('../routes/articles.routes'))
        this.app.use(this.paths.docs, swaggerUI.serve, swaggerUI.setup(this.swaggerDocs));
    }

    async requestPage(){
        try {
            const response = await axios.get('https://hn.algolia.com/api/v1/search_by_date?query=nodejs')
            this.data = await response.data.hits
            this.data.map(async article => {
                const articleDB = await Article.findOne({objectID: article.objectID})
                if(!articleDB){
                    const newArticle = new Article({...article})
                    await newArticle.save()
                }
            })
            console.log(`The data base has been updated`);
        } catch (error) {
          console.error(error)
        }
    }

    requestBucle(){
        setInterval(async ()=>{
            this.requestPage()
        },1000*60*60)
    }

    listen(){
        if (process.env.NODE_ENV !== "test") {
            this.app.listen(this.port, () => {
                console.log(`listening on the port ${this.port}`)
            })
        }
    }

    close(){
        this.app.close()
    }
}

module.exports = Server
