const mongoose = require('mongoose')

const dbConnection = async() =>{
    try{
        await  mongoose.connect(process.env.MONGO_CONNECTION,{
            useNewUrlParser: true,
            useUnifiedTopology: true,
        })
        console.log('Data base online...');
    }catch(err){
        console.log(err);
        throw new Error(`Error - ${err}`)
    }
}

module.exports = {dbConnection}
