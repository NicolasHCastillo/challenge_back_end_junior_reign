const Article =  require("../../models/article.models");

describe('Article Model', () => {

    const param = {
        created_at: 'CREATED',
        delete: true,
        title: 'TITLE',
        url: 'UCN.CL',
        points: 1111,
        story_text: 'STORY TEXT',
        comment_text: 'COMMENT TEXT',
        num_comments: 1234,
        story_id: 1,
        story_title: 'ST0RY TITLE',
        story_url: 'STORY URL',
        parent_id: '1',
        created_at_i: '-',
        _tags: ['a', 'b'],
        objectID: 'AADSADASDFADASDASNKDNSAKJDNSKADJABSDJHAS',
        _highlightResult: {},
    }

    test('should execute the json to object function', () => {
        const articleEntity = new Article(param)
        const articleJson = articleEntity.toJSON();

        expect(articleJson).toBeDefined();
        expect(articleJson).not.toHaveProperty(['__v'])
    });
});
