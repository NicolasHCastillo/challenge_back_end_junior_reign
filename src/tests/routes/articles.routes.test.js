const article = require('../../controllers/articles.controllers');
const Article =  require("../../models/article.models");

describe("Articles", () => {

  describe('filter article', () => {
    test('it should get a valid article with all params given', async () => {
      const requestParam = {
        query: {
          author: 'SANDERSON',
          _tags: 'xbox, ps5, switch',
          title: 'STORMLIGTH ARCHIVE',
        }
      };

      const jsonFn =  {
        json:(value) => {}
      }
  
      const responseParams = {
        status: () => jsonFn
      }

      const responseFind = {
        content: 'data'
      };

      const findSpyOn = jest.spyOn(Article, 'paginate')
                                                .mockResolvedValue({
                                                  content: 'data'
                                                })
      const statusSpyOn = jest.spyOn(responseParams, 'status');
      const jsonSpyOn = jest.spyOn(jsonFn, 'json');

      await article.filterArticles(requestParam, responseParams);

      const [ findObj ] = findSpyOn.mock.calls[0];
      const { title, _tags, delete: deleteParam } = findObj;
      expect(findObj['_highlightResult.author.value']).toEqual( RegExp(requestParam.query.author, 'i') );
      expect(title.$in).toEqual( RegExp(requestParam.query.title, 'i') );
      expect(deleteParam).toEqual( false );
      
      const arrTags = requestParam.query._tags.split(',')
      const tags = arrTags.map(tag =>  RegExp(tag, 'i'))
      expect(_tags.$in).toEqual(tags);
      
      const [ statusNumber ] = statusSpyOn.mock.calls[0];
      expect(statusNumber).toEqual(200)
  
      const [ jsonObj ] = jsonSpyOn.mock.calls[0];
      const { data } = jsonObj;
      expect(data).toEqual(responseFind);
    })

    test('it should get a valid article with no params given', async () => {
      const requestParam = {
        query: {}
      };

      const jsonFn =  {
        json:(value) => {}
      }
  
      const responseParams = {
        status:(statusValue) => jsonFn
      }

      const responseFind = [];

      const findSpyOn = jest.spyOn(Article, 'paginate').mockResolvedValue([])
      const statusSpyOn = jest.spyOn(responseParams, 'status');
      const jsonSpyOn = jest.spyOn(jsonFn, 'json');

      await article.filterArticles(requestParam, responseParams);

      const [ filterObj ] = findSpyOn.mock.calls[0];
      const { title, _tags, delete: deleteParam } = filterObj;

      expect(filterObj['_highlightResult.author.value']).toEqual( [null, /./] );
      expect(title.$in).toEqual( [null, /./] );
      expect(deleteParam).toEqual( false );
      expect(_tags.$in).toEqual( [null, /./] );
      
      const [ statusNumber ] = statusSpyOn.mock.calls[0];
      expect(statusNumber).toEqual(200)
  
      const [ jsonObj ] = jsonSpyOn.mock.calls[0];
      const { data } = jsonObj;
      expect(data).toEqual(responseFind);
    })
    test('should throw an error when can not get an article from db', async () => {
      const requestParam = {
        query: {}
      };
      const responseParams = {}
      const findSpyOn = jest.spyOn(Article, 'paginate').mockResolvedValue({
                                                                      content: "data"
                                                                      })

      await expect(article.filterArticles(requestParam, responseParams)).rejects.toThrow();
      
      expect(findSpyOn).toHaveBeenCalled();
    })

    test('should return status 400 when page isnt a number', async()=>{
      const MESSAGE_ARTICLE_TAG_ERROR = 'page most be a number'
      const requestParam = {
        query: {
          page: "value"
        }
      };

      const jsonFn =  {
        json:(value) => {}
      }
  
      const responseParams = {
        status:(statusValue) => jsonFn
      }

      const statusSpyOn = jest.spyOn(responseParams, 'status');
      const jsonSpyOn = jest.spyOn(jsonFn, 'json');

      await article.filterArticles(requestParam, responseParams)

      const [ status ] = statusSpyOn.mock.calls[0]
      expect(status).toEqual(400)

      const [ jsonObj ] = jsonSpyOn.mock.calls[0]
      expect(jsonObj.msg).toEqual(MESSAGE_ARTICLE_TAG_ERROR)
    })

  });

  describe('deleteArticle', () => {

    test('Should get a valid article in the db and return it (Case1: Good path)', async()=>{
      const articleResponse = {
          _id: "id",
          created_at: "2022-02-07T08:58:48.000Z",
          delete: true,
          title: null,
          url: null,
          points: null,
          story_text: null,
          comment_text: "thats is a comment.",
          num_comments: null,
          story_id: 1234,
          story_title: "that is a story title",
          story_url: "https://URL.com",
          parent_id: "30241148",
          created_at_i: "1644224328",
          _tags: [
              "comment",
              "author_starfallg",
              "story_30228178"
          ],
          objectID: "30242069",
          _highlightResult: {
              author: {
                  value: "author name",
                  matchLevel: "none",
                  matchedWords: []
              },
              comment_text: {
                  value: "comment text",
                  matchLevel: "full",
                  fullyHighlighted: false,
                  matchedWords: [
                      "nodejs"
                  ]
              },
              story_title: {
                  value: "Story title",
                  matchLevel: "none",
                  matchedWords: []
              },
              story_url: {
                  value: "https://URL.com",
                  matchLevel: "none",
                  matchedWords: []
              }
          }
      }
      const deleteSpyOn = jest.spyOn(Article,'findByIdAndUpdate').mockResolvedValue(articleResponse)
  
      const requestParam  = {
        params: {
           id:"ID"
        }
      }
  
      const jsonFn =  {
        json:(value) => {}
      }
  
      const responseParams = {
        status:(statusValue)=>{
          return jsonFn;
        }
      }

      const statusSpyOn = jest.spyOn(responseParams, 'status');
      const jsonSpyOn = jest.spyOn(jsonFn, 'json');
  
      await article.deleteArticle(requestParam, responseParams)
  
      const [ID, deleteObj, newObj] = deleteSpyOn.mock.calls[0];
      expect(ID).toEqual(requestParam.params.id);
      expect(deleteObj.delete).toEqual(true);
      expect(newObj.new).toEqual(true);
  
      const [ statusNumber ] = statusSpyOn.mock.calls[0];
      expect(statusNumber).toEqual(200)
  
      const [ jsonObj ] = jsonSpyOn.mock.calls[0];
      const { msg } = jsonObj;
      expect(msg).toContain(requestParam.params.id);
      expect(msg).toEqual(`The article with id ${requestParam.params.id} was delete`);
    })

    test('Should return a status 404 when the article doesnt exists in the db', async () => {
      const deleteSpyOn = jest.spyOn(Article,'findByIdAndUpdate').mockResolvedValue(null)
      const requestParam  = {
        params: {
           id:"ID"
        }
      }
  
      const jsonFn =  {
        json:(value) => {}
      }
  
      const responseParams = {
        status:(statusValue)=>{
          return jsonFn;
        }
      }

      const statusSpyOn = jest.spyOn(responseParams, 'status');
      const jsonSpyOn = jest.spyOn(jsonFn, 'json');

      await article.deleteArticle(requestParam, responseParams);

      expect(statusSpyOn).toHaveBeenCalled();
      expect(jsonSpyOn).toHaveBeenCalled();

      const [ status ] = statusSpyOn.mock.calls[0];
      expect(status).toEqual(404);

      const [ jsonObj ] = jsonSpyOn.mock.calls[0];
      const { msg } = jsonObj;
      expect(msg).toEqual("Article not found");
    });

  });

  afterEach(() => {
    jest.clearAllMocks();
  })
});
