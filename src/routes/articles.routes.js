const {Router} = require('express')
const { deleteArticle, filterArticles} = require('../controllers/articles.controllers')
const { validarCampos } = require('../middlewares/validate.middleware')
const {check} = require('express-validator')

const router = Router()

/**
 * 
 * @swagger
 * /Articles:
 * paths:
 *  /api/article:
 *   get:
 *     description: Get articles from the db using filtered by author, _tags, title 
 *     tags: [Articles API]
 *     parameters:
 *      - in : query
 *        name: author
 *        description: the name of the author that want to filter
 *        type: string
 *      - in : query
 *        name: _tags
 *        description: the tags that want to filter, if you want to use more that one tag, concatenate using a comma
 *        type: string
 *      - in : query
 *        name: title
 *        description: the title name that want to filter
 *        type: string
 *      - in: query
 *        name: page
 *        description: the number of the page you want to get, default value 1
 *        type: integer
 *     responses:
 *       200:
 *         description: Articles
 */
router.get('/',filterArticles)


//delete a article using the mongoid
/**
 * @swagger
 * /Articles:
 * paths:
 *  /api/article/{mongoId}:
 *   delete:
 *     description: Delete a article from the db using a valid mongoID
 *     tags: [Articles API]
 *     parameters:
 *      - in : path
 *        name: mongoId
 *        description: Id of the article in format mongoId
 *        type: string
 *        required: true
 *     responses:
 *       200:
 *         description: Article deleted
 */
router.delete('/:id',[
    check('id','id most be a mongoId').isMongoId(),
    validarCampos
],deleteArticle)

module.exports = router
